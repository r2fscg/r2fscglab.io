#!/usr/bin/env python
# Format markdown docs. For example, insert whitespaces to English and Chinese words

import re
import sys
import codefast as cf
from codefast.cn import is_cn

if len(sys.argv) < 2 or not cf.io.exists(sys.argv[1]):
    raise Exception("Usage: python %s <input_file>" % sys.argv[0])

doc = sys.argv[1]
text = cf.io.reads(doc)

pre, resp = '', ''
for i, c in enumerate(text):
    c_added = False
    if is_cn(c) and pre:
        if re.search(r'[a-zA-Z0-9]', pre):
            resp += ' ' + c
            c_added = True

    if is_cn(c) and i < len(text) - 1 and re.search(r'[a-zA-Z0-9]', text[i + 1]):
        resp += c + ' '
        c_added = True

    if not c_added:
        resp += c
    pre = c

print(resp)
