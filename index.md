---
layout: page
title: Index
permalink: /index/
---

- [为什么你需要习惯而不是激情 / HappyXiao letter](https://xiao.do/issues/079-1000344)
- [paddlepedia, 深度学习百科及面试资源](https://paddlepedia.readthedocs.io/en/latest/index.html)
- [科学空间](https://kexue.fm/)

