---
layout: post
title:  "wav2vec"
date:   2022-01-24 15:32:14 -0300
categories: ftt, facebook
---

xxx
Are 


基于标注语音数据的语音识别模型，特别是深度学习模型进展迅速，效果也较好。

但这些模型存在一个缺陷：它们需要大量标注数据，且这些数据仅针对英文和少数几种语言。纯监督式的训练对于全球 7000 种语言中的绝大多数是不可行的，因此如何更好地利用无标注语音数据是一个值得探索的问题。 

利用无标注数据的方法包括:
- 经典的自训练，这类方法对**无标注音频数据进行伪标注**，并使用额外标注数据对系统进行重新训练。
- 另一类工作是先在**无标注语音数据上预训练表征，然后在标注数据上进行微调**。

Facebook 的研究成果 [Self-training and Pre-training are Complementary for Speech Recognition](https://arxiv.org/abs/2010.11430) 将自训练和无监督预训练结合起来。这两种利用无标注数据的方法在基准上都取得了不错的结果，该研究想要解决的核心问题是它们**能否互补**。


### 效果
在 Librispeech 完整数据集和 Librilight 低资源标注数据设置下，自训练和无监督预训练具备互补性，这与近期自然语言理解领域的研究结果一致。仅使用 10 分钟的标注数据和 LibriVox 无标注数据，wav2vec 2.0 和自训练方法的结合体就在 Librispeech clean 和 other 测试集上取得 3.0%/5.2% 的词错率，相比仅使用预训练方法的近期研究词错率分别降低了 25% 和 40%。


## XLS-R

[XLS-R](https://ai.facebook.com/blog/xls-r-self-supervised-speech-processing-for-128-languages/) 
基于wav2vec 2.0 (2020) 架构，它使用卷积神经网络（CNN）特征编码器将音频转换为潜在的语音表示，这些表示经过量化后被送入转化器（Transfomer）。在训练过程中，输入的spans被屏蔽，模型的目标是识别 masked input 的量化表示。训练后的模型是音频输入的编码器；对于下游任务，编码器的输出可以被送到线性层进行语音分类和识别，或者送到解码器进行翻译。

![xls-r](https://s2.loli.net/2022/01/25/myEhOBQAUY12fPg.gif)

与BERT类似，XLS-R 是通过预测音频mask部分的语音单元来训练的。它们的区别是，语音音频是一种连续的信号，不能轻易清晰地分割成单词或其他单位。wav2vec 2.0通过学习25毫秒长的基本单元来解决这个问题，以便能够学习高级上下文表示。

在仅拥有一小时的标记训练数据的情况下，wav2vec 2.0能通过后续无监督的训练数据，在LibreSpeech测试基准的100小时子集上达到SOTA水平。

之后，Facebook又推出了完全无监督的高性能语音识别模型wav2vec-U，它纯粹从录制的语音音频和未配对的文本中学习。 
为了wav2vec-U让学习识别音频录音中的单词，Facebook训练了一个GAN。生成器根据嵌入在自监督表示中的每个音频段，预测与语言中的声音对应的音素。

XLS-R只是一个预训练模型，为了能更好的服务于具体任务，还需要对模型进行微调。HuggingFace 提供一个微调示例： [https://huggingface.co/blog/fine-tune-xlsr-wav2vec2](https://huggingface.co/blog/fine-tune-xlsr-wav2vec2)。

### Stories
- 2B parameters
- Trained on more than 436,000 hours of publicly available speech recordings, including [VoxPopuli](https://aclanthology.org/2021.acl-long.80/) 10 times more than the previous model FAIR XLSR-53
  

## References:
- [Facebook models on Huggingface](https://huggingface.co/models?other=xls_r)
- [paper: XLS-R: Self-supervised Cross-lingual Speech Representation Learning at Scale](https://arxiv.org/abs/2111.09296)
- [Facebook's Wav2Vec2 XLS-R fine-tuned for Speech Translation](https://huggingface.co/facebook/wav2vec2-xls-r-2b-22-to-16)
- [Hugging Face XLS-R 2B 22-to-16 Speech Translation APP](https://huggingface.co/spaces/facebook/XLS-R-2B-22-16)
- [FAIR新研究取得语音识别大进展](https://blog.ailemon.net/2020/11/06/paper-arxiv-2010-11430/)
- [qbitai: Meta 发布支持128种语言的新语音模型](https://www.qbitai.com/2021/11/30363.html)
