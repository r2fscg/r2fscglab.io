---
title: Extra trees classifier
subtitle: Notes
date: 2022-02-18
tags: ["extra trees"]
---

Extra Trees(Extremly Randomized Trees)

<!--more-->
## 工作方式
- 创建多个决策树，使用整个数据集进行训练
- 结果预测：回归任务使用预测均值，分类任务硬 vote

> The predictions of the trees are aggregated to yield the final prediction, by majority vote in classification problems and arithmetic average in regression problems. 
>   — [Extremely Randomized Trees](https://link.springer.com/article/10.1007/s10994-006-6226-1), 2006.


Extra Trees v.s. Random Forest

| X            | Random Forest             | Extra Trees   |
| :----------- | :------------------------ | :------------ |
| ensemble way | boostrap replicas         | whole samples |
| cut points   | chooses the optimum split | randomly      |

区别: 
- Random forest 从样本中抽样进行训练，而 extra trees 使用所有样本
- 运行速度上， extra trees 速度要**快**一些，因为在拆分节点时不需要考虑最优分割点，而是随机选择分割点。详细可参考[Difference between Extra Trees and Random Forest](https://quantdare.com/what-is-the-difference-between-extra-trees-and-random-forest/)
  - 注：以上文章中对随机生成的数据进行训练预测，random forest 结果要稍好一点

相同点：
- 都会随机采样特征子集



# References
- [Geurts, Pierre, Damien Ernst, and Louis Wehenkel. “Extremely randomized trees.” Machine learning 63.1 (2006): 3-42.](https://link.springer.com/article/10.1007/s10994-006-6226-1)