---
title: Collections
date: 2022-02-12
tags: ["links", "thoughts"]
---

# NLP

- [约翰斯·霍普金斯大学 nlp reading group](https://wiki.clsp.jhu.edu/index.php/NLP_Reading_Group)

<!--more-->


# Talks
- [The first 20 hours -- how to learn anything | Josh Kaufman | TEDxCSU](https://www.youtube.com/watch?v=5MgBikgcWnY)


# ML
- [cross validate 不是集成，尽管你可以拿来做集成](https://stats.stackexchange.com/questions/411290/how-to-use-a-cross-validated-model-for-prediction)


# Visualize
- [kaggle 上一个kernel，如何展示聚类图](https://www.kaggle.com/remekkinas/super-learner-ensemble-extree-tuned-lda-umap)
